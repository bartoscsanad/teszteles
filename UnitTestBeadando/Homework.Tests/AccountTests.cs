﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.Tests
{
    class AccountTests
    {
        private Account _account;
        private Mock<IAction> _action;

        [SetUp]
        public void SetUp()
        {
            _account = new Account(0);
            _action = new Mock<IAction>();
        }

        [Test]
        [Category("Register")]
        public void Register_SuccesfullyCreated()
        {
            _account.Register();
            Assert.That(_account.IsRegistered, Is.True);
        }

        [Test]
        [Category("Activate")]
        public void Activate_SuccesfullyActivated()
        {
            _account.Activate();
            Assert.That(_account.IsConfirmed, Is.True);
        }

        [Test]
        [Category("TakeAction")]
        public void InactiveAccount_TakeAction_ThrowsInactiveUserException()
        {
            Assert.That(() => _account.TakeAction(_action.Object), Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        [Category("TakeAction")]
        public void ValidAccount_TakeAction_IncrementActionsSuccesfullyPeformed()
        {
            _account.Register();
            _action.Setup(action => action.Execute()).Returns(true);
            bool success = _account.TakeAction(_action.Object);

            Assert.That(success, Is.True);
            Assert.That(_account.ActionsSuccessfullyPerformed, Is.EqualTo(1));

        }

    }
}

﻿using System;
using System.Collections.Generic;
using Homework.ThirdParty;
using Moq;
using NUnit.Framework;

namespace Homework.Tests
{
    public class AccountActivityServiceTests
    {

        private AccountActivityService _accountActivityService;
        //private FakeAccountRepository _accountRepository;
        private Mock<IAccountRepository> _accountRepository;
        private int accountId1;
        Account account1;


       [SetUp]
        public void SetUp()
        {
            _accountRepository = new Mock<IAccountRepository>();
            _accountActivityService = new AccountActivityService(_accountRepository.Object);

            accountId1 = 0;
            account1 = new Account(accountId1);
            _accountRepository.Object.Add(account1);
            _accountRepository.Setup<Account>(acc => acc.Get(accountId1)).Returns(account1);
        }

        [Test]
        [Category("GetActiity")]
        public void InvalidAccount_GetActivity_ThrowsAccountNotExistsException()
        {
            int accountId = 3;
            Assert.That(() => _accountActivityService.GetActivity(accountId), Throws.TypeOf<AccountNotExistsException>());
        }

        [Test]
        [Category("GetActiity")]
        public void ValidAccountZeroActionPerformed_GetActivity_ReturnsActivityLevelNone()
        {
            account1.ActionsSuccessfullyPerformed = 0;

            Assert.That(() => _accountActivityService.GetActivity(accountId1),
                Is.EqualTo(ActivityLevel.None));

            _accountRepository.Verify(repo => repo.Get(accountId1), Times.Once);
        }
        [Test]
        [Category("GetActiity")]
        public void ValidAccountOneActionPerformed_GetActivity_ReturnsActivityLevelLow()
        {
            account1.ActionsSuccessfullyPerformed = 1;

            Assert.That(() => _accountActivityService.GetActivity(accountId1),
                Is.EqualTo(ActivityLevel.Low));
        }
        [Test]
        [Category("GetActiity")]
        public void ValidAccountTwentyActionPerformed_GetActivity_ReturnsActivityLevelMedium()
        {
            account1.ActionsSuccessfullyPerformed = 20;

            Assert.That(() => _accountActivityService.GetActivity(accountId1),
                Is.EqualTo(ActivityLevel.Medium));
        }
        [Test]
        [Category("GetActiity")]
        public void ValidAccountFortyActionPerformed_GetActivity_ReturnsActivityLevelHigh()
        {
            account1.ActionsSuccessfullyPerformed = 40;

            Assert.That(() => _accountActivityService.GetActivity(accountId1),
                Is.EqualTo(ActivityLevel.High));
        }

        [Test]
        [Category("GetAmountForActivity")]
        public void GetAmountForActivity_ReturnsCorrectAmount()
        {
            account1.ActionsSuccessfullyPerformed = 0;
            int accountId2 = 1;
            Account account2 = new Account(accountId2);
            account2.ActionsSuccessfullyPerformed = 40;
            _accountRepository.Object.Add(account2);
            _accountRepository.Setup<Account>(acc => acc.Get(accountId2)).Returns(account2);

            List<Account> accs = new List<Account>();
            accs.Add(account1);
            accs.Add(account2);
            _accountRepository.Setup(acc => acc.GetAll()).Returns(accs);

            Assert.That(_accountActivityService.GetAmountForActivity(ActivityLevel.None), Is.EqualTo(1));
            Assert.That(_accountActivityService.GetAmountForActivity(ActivityLevel.Low), Is.EqualTo(0));
            Assert.That(_accountActivityService.GetAmountForActivity(ActivityLevel.Medium), Is.EqualTo(0));
            Assert.That(_accountActivityService.GetAmountForActivity(ActivityLevel.High), Is.EqualTo(1));
        }
    }
}
